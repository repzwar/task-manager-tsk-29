package ru.pisarev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.dto.Domain;
import ru.pisarev.tm.enumerated.Role;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;

import static ru.pisarev.tm.constant.JaxBConst.*;

public class DataJsonLoadJaxBCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String name() {
        return "data-load-json-j";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @Nullable
    @Override
    public String description() {
        return "Load data from JSON by JaxB.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.setProperty(FACTORY, JAXBFACTORY);
        @NotNull final File file = new File(FILE_JSON_JAXB);
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        unmarshaller.setProperty(TYPE, JSON);
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
        setDomain(domain);
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
